
# snpt - The terminal-based snippet archive.

### What is snpt?
snpt, is an open source terminal-based snippet archive that designed for the modern HTTP command-line clients such as cURL.

### Firstly, send the command below from your terminal:
```
$> snpt(){ curl -s https://snpt.gitlab.io/${1};}
```

### Optional, add this alias to your .bashrc:
```
$> echo 'snpt(){ curl -s https://snpt.gitlab.io/${1};}' | sudo tee -a ~/.bashrc 
```

### Get help:
```
$> snpt
```

### Get all tags:
```
$> snpt tags
```

### Get all snippets:
```
$> snpt all
```

### Search in snippets:
```
$> snpt all | grep term
```

### Get snippets by tag:
```
$> snpt tag/mysql
```

### Get snippet:
```
$> snpt hello-world
```

### How to contribute?
1. Fork the repository: https://gitlab.com/snpt/snpt.gitlab.io
2. Add some good snippets, then send a pull request.

### Creator of snpt:
Ekin Karadeniz <<iamdual@protonmail.com>>