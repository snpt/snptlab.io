# snpt - The terminal-based snippet archive.
# Author: Ekin Karadeniz <iamdual@protonmail.com>

import os, sys, glob
sys.path.append(os.curdir)

if not os.path.exists("public"):
  os.makedirs("public")

if not os.path.exists("public/tag"):
  os.makedirs("public/tag")    

if not os.path.exists("snippets"):
  print("The 'snippets' directory not found.")
  os.exit(1)

tags_dict = {}
tags_list = []
snpt_list = []
reserved  = ["index", "about", "404", "tag", "tags", "all", "about"]

class color:
  BOLD = '\033[1m'
  GREEN = BOLD + '\033[92m'
  BLUE = BOLD + '\033[94m'
  RED = BOLD + '\033[91m'
  END = '\033[0m'
  BG_GREEN = '\033[1;42m'
  BG_RED = '\033[1;41m'
  BG_END = '\033[1;m'

for filename in glob.iglob('snippets/*.txt'):
  slug = filename[9:-4] # len("snippets") : - len(".txt")
  if slug in reserved:
    continue

  if slug[0] == "_":
    slug = slug[1:]

  lnum = 0
  with open(filename) as lines:
    content = ""
    for line in lines.readlines():
      lnum = lnum + 1
      if lnum == 1 and slug not in ["index", "about", "404"]:
        header = line.split("[")
        title  = header[0].strip()
        tags   = None

        snpt_list.append((slug, title))

        if len(header) > 1:
          tags = header[1].strip()[:-1] # Trim "]"
          for tag in tags.replace(" ", ",").split(","):
            if len(tag) > 0:
              tags_dict.setdefault(tag,[]).append((slug, title))

        content += color.BG_GREEN + title + color.BG_END + "\n"
      else:
        content += line.replace("[BOLD]", color.BOLD).replace("[GREEN]", color.GREEN).replace("[BLUE]", color.BLUE).replace("[RED]", color.RED).replace("[/END]", color.END).replace("[BG_GREEN]", color.BG_GREEN).replace("[BG_RED]", color.BG_RED).replace("[/BG_END]", color.BG_END)
      
      if slug in ["index", "404"]:
        slug = slug + ".html"
      with open(("public/" + slug), "w") as file:
        file.write(content.strip() + "\n")

for tag in tags_dict:
  total    = len(tags_dict[tag])
  txt_snpt = "snippet"
  if total > 1: 
    txt_snpt = "snippets"
  content  = color.BG_GREEN + "Tags > "+tag+" ("+str(total)+" "+txt_snpt+")" + color.BG_END + "\n\n"
  content += color.GREEN + "Type command to get snippet: snpt <snippet-id>" + color.END + "\n\n"
  tags_list.append((tag, total))
  for snpt in tags_dict[tag]:
    slug  = snpt[0]
    title = snpt[1]
    content += slug + " " + color.BLUE + "(" + title + ")" + color.END + "\n"
    with open(("public/tag/" + tag), "w") as file:
      file.write(content.strip() + "\n")

tags_list = sorted(tags_list, key=lambda tup: (tup[1]), reverse=True)

content  = color.BG_GREEN + "All tags (" + str(len(tags_list)) + " total)" + color.BG_END + "\n\n"
content += color.GREEN + "Type command to get tag: snpt tag/<tag-id>" + color.END + "\n\n"
for tag in tags_list:
  name  = tag[0]
  total = tag[1]
  content += name + " " + color.BLUE + "(" + str(total) + ")" + color.END + ", "
with open(("public/tags"), "w") as file:
  file.write(content[:-2].strip() + "\n")

content  = color.BG_GREEN + "All snippets (" + str(len(snpt_list)) + " total)" + color.BG_END + "\n\n"
content += color.GREEN + "Type command to get snippet: snpt <snippet-id>" + color.END + "\n\n"
for snpt in snpt_list:
  slug  = snpt[0]
  title = snpt[1]
  content += slug + " " + color.BLUE + "(" + title + ")" + color.END + "\n"
with open(("public/all"), "w") as file:
  file.write(content.strip() + "\n")