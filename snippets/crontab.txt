Basic crontab examples [crontab]

[GREEN]Edit crontab:[/END]
crontab -e

[GREEN]List crontab:[/END]
crontab -l

[GREEN]Crontab examples:[/END]
[BLUE]Execute on every minutes:[/END]
* * * * * /root/example.sh
[BLUE]Execute on every 10 minutes:[/END]
*/10 * * * * /home/example.sh
[BLUE]Execute on every 4 hours:[/END]
0 */4 * * * /home/example.sh
[BLUE]Execute on every 5 days:[/END]
0 0 */5 * * /home/example.sh
[BLUE]Execute on every 2 months:[/END]
0 0 1 */2 * /home/example.sh
[BLUE]Execute on hourly:[/END]
0 * * * * /home/example.sh
[BLUE]Execute on daily:[/END]
0 0 * * * /home/example.sh
[BLUE]Execute on monthly:[/END]
@monthly /home/example.sh
[BLUE]Execute on weekly:[/END]
@weekly /home/example.sh
[BLUE]Execute on yearly:[/END]
@yearly /home/example.sh
[BLUE]Execute on system reboot:[/END]
@reboot /home/example.sh
[BLUE]Execute at 2 AM daily:[/END]
0 2 * * * /home/example.sh
[BLUE]Execute on every Sunday at 5 PM:[/END]
0 17 * * sun /home/example.sh
[BLUE]Execute on every 30 seconds:[/END]
* * * * * /home/example.sh
* * * * * sleep 30; /home/example.sh