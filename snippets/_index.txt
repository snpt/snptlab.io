[BG_GREEN]snpt - The terminal-based snippet archive.[/BG_END]

[BLUE]What is snpt?[/END]
snpt, is an open source terminal-based snippet archive that designed for the modern HTTP command-line clients such as cURL.

[BLUE]Firstly, send the command below from your terminal:[/END]
[BLUE]$>[/END] snpt(){ curl -s https://snpt.gitlab.io/${1};}

[BLUE]Optionally, add snpt as alias to your .bashrc:[/END]
[BLUE]$>[/END] echo 'snpt(){ curl -s https://snpt.gitlab.io/${1};}' | sudo tee -a ~/.bashrc 

[BLUE]Get help:[/END]
[BLUE]$>[/END] snpt

[BLUE]Get all snippets:[/END]
[BLUE]$>[/END] snpt all

[BLUE]Search in snippets:[/END]
[BLUE]$>[/END] snpt all | grep -i term

[BLUE]Get all tags:[/END]
[BLUE]$>[/END] snpt tags

[BLUE]Get snippets by tag:[/END]
[BLUE]$>[/END] snpt tag/mysql

[BLUE]Get snippet:[/END]
[BLUE]$>[/END] snpt hello-world

[BLUE]About:[/END]
[BLUE]$>[/END] snpt about

[BLUE]Want to contribute?[/END]
We are here: https://gitlab.com/snpt/snpt.gitlab.io
