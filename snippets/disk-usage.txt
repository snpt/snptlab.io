Find disk usages [disk, files]

[GREEN]Remain disk space of the system:[/END]
df -h

[GREEN]Disk usages of the specific directories:[/END]
du -hs /var/www/*